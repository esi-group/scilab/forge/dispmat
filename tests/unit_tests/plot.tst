// Copyright (C) 2008 - 2010 - Michael Baudin

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

// Basic use
A=[
          -9          11         -21          63        -252
          70         -69         141        -421        1684
        -575         575       -1149        3451      -13801
        3891       -3891        7782      -23345       93365
        1024       -1024        2048       -6144       24572
];
newplot = dispmat_plotnew ( A );
cellij = dispmat_plotgetcell ( newplot , 2 , 3 );
cellij.FontAngle = "italic";
cellij.String = string(12);
cellij.BackgroundColor = [1 0 0];
close()

// Customized use
A=ceil(rand(3,3)*10);
newplot = dispmat_plotnew ( A , 200 , 200 , "Matrix A");
close()
// Display digits
A=[
0 1 0 1
];
newplot = dispmat_plotnew ( A , 400 , 50 );
close()
