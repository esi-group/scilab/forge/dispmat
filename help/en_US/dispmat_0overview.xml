<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * Copyright (C) 2010 - DIGITEO - Michael Baudin
-->
<refentry 
  xmlns="http://docbook.org/ns/docbook" 
  xmlns:xlink="http://www.w3.org/1999/xlink" 
  xmlns:svg="http://www.w3.org/2000/svg"  
  xmlns:mml="http://www.w3.org/1998/Math/MathML" 
  xmlns:db="http://docbook.org/ns/docbook" 
  version="5.0-subset Scilab" 
  xml:lang="fr" 
  xml:id="dispmat_overview">

  <refnamediv>
    <refname>Dispmat</refname>

    <refpurpose>An overview of the Dispmat toolbox.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Purpose</title>

    <para>
    The goal of this toolbox is to provide algorithms to display matrices.
    </para>

    <para>
    Several functions focus of the mathematical analysis of the numerical 
    properties of matrices. 
    For example, the <literal>dispmat_see</literal> function displays 4 plots which 
    allow to see the properties of the matrix and its pseudo-inverse. 
    The <literal>dispmat_gersh</literal> allows to see the Gershgoring circles 
    of the current matrix.
    </para>

    <para>
    Other functions allows to display graphically the entries of a matrix. 
    These functions may be used for example to illustrate the update 
    of a matrix during a Gaussian elimination algorithm. 
    The <literal>dispmat_plotnew</literal> displays a matrix of values with 
    entries from A.
    Once the graphics created, the <literal>dispmat_plotconf</literal> function allows to configure 
    the properties of the graphical object which correspond to the entry (i,j) of the matrix. 
    The more general <literal>dispmat_plotgetcell</literal> returns the handle corresponding 
    to the entry (i,j) and allows a complete customization of this graphical object.
    </para>

    <para>
    The toolbox is based on macros.
    </para>

    <para>
    This module depends on the apifun module.
    </para>

  </refsection>

  <refsection>
    <title>Quick start</title>

    <para>
    In the following example, we create a graphical 5 x 5 matrix.
    </para>

    <programlisting role="example"><![CDATA[ 
A=[
          -9          11         -21          63        -252
          70         -69         141        -421        1684
        -575         575       -1149        3451      -13801
        3891       -3891        7782      -23345       93365
        1024       -1024        2048       -6144       24572
];
graphmat = dispmat_plotnew ( A );
cellij = dispmat_plotgetcell(graphmat,2,3);
cellij.BackgroundColor = [1 0 0];
cellij.FontAngle = "italic";
cellij.String = string(12);
// See that beautiful matrix...
// ... then close:
close(graphmat.figure);
 ]]></programlisting>

    <para>
    In the following example, we show the numerical properties of a 5 x 5 matrix.
    </para>

    <programlisting role="example"><![CDATA[ 
A=[
          -9          11         -21          63        -252
          70         -69         141        -421        1684
        -575         575       -1149        3451      -13801
        3891       -3891        7782      -23345       93365
        1024       -1024        2048       -6144       24572
];
dispmat_show ( A , %t );
h1 = dispmat_see ( A );
h2 = dispmat_spec ( A );
h3 = dispmat_gersh ( A );
// See that beautiful matrix...
// ... then close:
close(h1);
close(h2);
close(h3);
 ]]></programlisting>
  </refsection>


  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>2008-2009 - INRIA - Michael Baudin</member>
      <member>2010 - DIGITEO - Michael Baudin</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>
    &#8220;Accuracy and Stability of Numerical Algorithms&#8221;, A Gallery of Test Matrices , 
    Nicolas Higham
    </para>

    <para>
      &#8220;The Test Matrix Toolbox for Matlab (Version 3.0)&#8221;, Nicolas Higham,
      Numerical Analysis Report No. 276, September 1995, 
      Manchester Centre for Computational Mathematics, 
      The University of Manchester
    </para>

    <para>
      &#8220;Algorithm 694: a collection of test matrices in MATLAB&#8221;, Nicolas Higham,
      ACM Transactions on Mathematical Software, Volume 17 ,  Issue 3  (September 1991),
      Pages: 289 - 305
    </para>


    <para>
      &#8220;Higham's Test Matrices &#8221;, John Burkardt, 2005, 
      http://people.sc.fsu.edu/~burkardt/m_src/test_matrix/test_matrix.html
    </para>

  </refsection>

</refentry>
