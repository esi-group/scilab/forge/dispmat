//
// This help file was automatically generated from dispmat_see.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of dispmat_see.sci
//

A=[
-9          11         -21          63        -252
70         -69         141        -421        1684
-575         575       -1149        3451      -13801
3891       -3891        7782      -23345       93365
1024       -1024        2048       -6144       24572
];
h = dispmat_see ( A );
// Take some time to see the matrix...
// ... now close
close ( h );
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "dispmat_see.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
