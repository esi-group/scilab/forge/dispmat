//
// This help file was automatically generated from dispmat_gersh.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of dispmat_gersh.sci
//

A = [
10 -1 0 1
0.2 8 0.2 0.2
1 1 2 1
-1 -1 -1 -11
];
dispmat_gersh(A)
spec(A)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "dispmat_gersh.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
