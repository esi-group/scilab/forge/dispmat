// This help file was automatically generated using helpupdate
// PLEASE DO NOT EDIT
demopath = get_absolute_file_path("dispmat.dem.gateway.sce");
subdemolist = [
"dispmat_spec", "dispmat_spec.sce"; ..
"dispmat_show", "dispmat_show.sce"; ..
"dispmat_see", "dispmat_see.sce"; ..
"dispmat_plotnew", "dispmat_plotnew.sce"; ..
"dispmat_plotgetcell", "dispmat_plotgetcell.sce"; ..
"dispmat_plotconf", "dispmat_plotconf.sce"; ..
"dispmat_gersh", "dispmat_gersh.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
