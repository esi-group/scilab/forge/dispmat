//
// This help file was automatically generated from dispmat_plotgetcell.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of dispmat_plotgetcell.sci
//

A=[
-9          11         -21          63        -252
70         -69         141        -421        1684
-575         575       -1149        3451      -13801
3891       -3891        7782      -23345       93365
1024       -1024        2048       -6144       24572
];
graphmat = dispmat_plotnew ( A );
uientry = dispmat_plotgetcell ( graphmat , 2 , 3 );
uientry.String = string(123456);
// Pick a cell at random, color it at random
for k = 1:5
i = grand(1,1,"uin",1,5);
j = grand(1,1,"uin",1,5);
uientry = dispmat_plotgetcell ( graphmat , i , j );
uientry.BackgroundColor = grand(1,3,"def");
end
// Take some time to see the matrix...
// ... now close
close ( graphmat.figure );
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "dispmat_plotgetcell.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
