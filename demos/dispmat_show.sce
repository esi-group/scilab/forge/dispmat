//
// This help file was automatically generated from dispmat_show.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of dispmat_show.sci
//

str = dispmat_show ( 2*rand(10,10)-1 )
dispmat_show ( 2*rand(10,10)-1 , %t );
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "dispmat_show.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
