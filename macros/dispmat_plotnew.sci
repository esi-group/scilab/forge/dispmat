// Copyright (C) 2008 - 2010 - Michael Baudin

function graphmat = dispmat_plotnew ( varargin ) 
  //   Displays a matrix of values with entries from A.
  // 
  // Calling Sequence
  // graphmat = dispmat_plotnew ( A );
  // graphmat = dispmat_plotnew ( A , figWidth );
  // graphmat = dispmat_plotnew ( A , figWidth , figHeight , titlestr );
  //
  // Parameters
  //   A : a n-by-m matrix of doubles
  //   figWidth : a 1x1 matrix of floating point integers, the width of the figure (default figWidth = 400)
  //   figHeight : a 1x1 matrix of floating point integers, the width of the figure (default figHeight = 400)
  //   titlestr : a 1x1 matrix of strings, the title of the graphics (default titlestr = "Matrix Content")
  //   graphmat : a graphical matrix, as created by dispmat_plotnew
  //
  // Description
  //   Returns a data structure which can be processed by dispmat_plotgetcell.
  //
  // Examples
  // A=[
  //           -9          11         -21          63        -252
  //           70         -69         141        -421        1684
  //         -575         575       -1149        3451      -13801
  //         3891       -3891        7782      -23345       93365
  //         1024       -1024        2048       -6144       24572
  // ];
  // graphmat = dispmat_plotnew ( A );
  // // Take some time to see the matrix...
  // // ... now close
  // close ( graphmat.figure );
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "dispmat_plotnew" , rhs , 1:4 )
  apifun_checklhs ( "dispmat_plotnew" , lhs , 1 )
  //
  A = varargin(1)
  figWidth = argindefault ( rhs , varargin , 2 , 400 )
  figHeight = argindefault ( rhs , varargin , 3 , 400 )
  titlestr = argindefault ( rhs , varargin , 4 , "Matrix Content" )
  //
  apifun_checktype ( "dispmat_plotnew" , A , "A" , 1 , "constant" )
  apifun_checktype ( "dispmat_plotnew" , figWidth , "figWidth" , 2 , "constant" )
  apifun_checktype ( "dispmat_plotnew" , figHeight , "figHeight" , 3 , "constant" )
  apifun_checktype ( "dispmat_plotnew" , titlestr , "titlestr" , 4 , "string" )
  //
  apifun_checkscalar ( "dispmat_plotnew" , figWidth , "figWidth" , 2 )
  apifun_checkscalar ( "dispmat_plotnew" , figHeight , "figHeight" , 3 )
  apifun_checkscalar ( "dispmat_plotnew" , titlestr , "titlestr" , 4 )
  //
  apifun_checkgreq ( "dispmat_plotnew" , figWidth , "figWidth" , 2 , 1 )
  apifun_checkgreq ( "dispmat_plotnew" , figHeight , "figHeight" , 3 , 1 )
  //
  // Compute the number of rows, columns
  ncols = size(A,"c")
  nrows = size(A,"r")
  // Create the list of uicontrols
  // A(i,j) is represented by l((i-1)*n + j)
  graphmat = tlist(["T_DMPLOT" 
    "figure" 
    "uicontrols"
    "A"
    ]);
  graphmat.A = A
  cellWidth = figWidth / ncols
  cellHeight = figHeight / nrows
  fig=figure("Position",[10 10 figWidth figHeight]);
  fig.auto_resize = "off";
  set(fig,"figure_name",titlestr);
  graphmat.figure = fig
  graphmat.uicontrols = list()
  for i = 1 : nrows
    for j = 1 : ncols
      x = 0 + (j-1) * cellWidth
      y = figHeight - i * cellHeight
      h =uicontrol ( fig , ...
      "style" , "text" , ...
      "position" , [x y cellWidth cellHeight] , ...
      "BackgroundColor",[0.9 0.9 0.9], ...
      "Horizontalalignment","center",...
      "Relief" , "raised", ...
      "FontSize", 20, ...
      "String",string(A(i,j)) );
      graphmat.uicontrols((i-1)*ncols + j) = h;
    end
  end
endfunction

function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the 
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

