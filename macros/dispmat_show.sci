// Copyright (C) 2008 - 2010 - Michael Baudin

function str = dispmat_show ( varargin ) 
  //   Displays a matrix of +,- representing the sign of entries.
  // 
  // Calling Sequence
  // str = dispmat_show ( A ) 
  // str = dispmat_show ( A , verbose ) 
  //
  // Parameters
  //   A : a n-by-m matrix of doubles
  //   verbose : a 1x1 matrix of booleans, set to true to print the matrix (default = %f)
  //   this : a graphical matrix, as created by dispmat_plotnew
  //
  // Description
  //   Displays a
  //   <itemizedlist>
  //   <listitem>"+" for a positive value</listitem>
  //   <listitem>"-" for a negative value</listitem>
  //   <listitem>" " for a zero</listitem>
  //   </itemizedlist>
  //
  // Examples
  // str = dispmat_show ( 2*rand(10,10)-1 )
  // dispmat_show ( 2*rand(10,10)-1 , %t );
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "dispmat_show" , rhs , 1:2 )
  apifun_checklhs ( "dispmat_show" , lhs , 1 )
  //
  A = varargin(1)
  verbose = argindefault ( rhs , varargin , 2 , %f )
  //
  apifun_checktype ( "dispmat_show" , A , "A" , 1 , "constant" )
  apifun_checktype ( "dispmat_show" , verbose , "verbose" , 2 , "boolean" )
  //
  apifun_checkscalar ( "dispmat_show" , verbose , "verbose" , 2 )
  //
  m = size(A,"r")
  n = size(A,"c")
  str = []
  for i = 1:m
    str(i) = ""
    for j = 1:n
      if ( A(i,j) > 0 ) then
        str(i) = str(i) + msprintf("+");
      elseif ( A(i,j) < 0 ) then
        str(i) = str(i) + msprintf("-");
      else
        str(i) = str(i) + msprintf(" ");
      end
    end
  if ( verbose ) then
    mprintf("%s\n",str(i));
  end
  end
  
  if ( verbose ) then
    mprintf("\n");
  end
endfunction

function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the 
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

