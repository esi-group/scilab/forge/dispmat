// Copyright (C) 2008 - 2010 - Michael Baudin

function h = dispmat_spec ( A )
  //   Plots the eigenvalues of the matrix.
  // 
  // Calling Sequence
  // h = dispmat_spec ( A )
  //
  // Parameters
  //   A : a n-by-m matrix of doubles
  //   h : a graphics handle
  //
  // Description
  //   Plots the eigenvalues of the matrix. 
  //   The coordinates of each eigenvalue are its real and imaginary 
  //   parts.
  //
  // Examples
  // A=[
  //           -9          11         -21          63        -252
  //           70         -69         141        -421        1684
  //         -575         575       -1149        3451      -13801
  //         3891       -3891        7782      -23345       93365
  //         1024       -1024        2048       -6144       24572
  // ];
  // h = dispmat_spec(A);
  // close(h);
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "dispmat_spec" , rhs , 1 )
  apifun_checklhs ( "dispmat_spec" , lhs , 1 )
  //
  apifun_checktype ( "dispmat_spec" , A , "A" , 1 , "constant" )
  //

  h = scf()
  s = spec(A)
  plot(real(s),imag(s),"xg")
endfunction


