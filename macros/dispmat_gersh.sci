// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function h = dispmat_gersh ( A )
  // Draw the Gershgorin circles of the matrix. 
  // 
  // Calling Sequence
  //   h = dispmat_gersh ( A )
  //
  // Parameters
  //   A : a nxn matrix of doubles
  //   h : graphics handle
  //
  // Description
  //   Plots the Gershgorin circles of the matrix. 
  //   We assume that A is a n-by-n square matrix. 
  //   The matrix may be either real or complex. 
  //   Hence, there are n circles. 
  //   Each circle i=1,2,...,n has center (x_i,y_i), 
  //   where x_i and y_i are the real and imaginary parts of 
  //   the diagonal element A(i,i).
  //   The radius of each circle is the sum of the absolute value of the 
  //   off-diagonal entries in the row i.
  //   This can be computed with the vectorized statement: 
  //   <programlisting>
  //     r(i) = sum(abs(A(i,:))) - abs(A(i,i))
  //   </programlisting>
  //
  //   The Gershgoring circle theorem states that every eigenvalue of A 
  //   lies within at least one of the Gershgorin discs.
  //   If the union #1 of k discs is disjoint from the union #2 of the other n ? k discs 
  //   then the union #1 contains exactly k and the union #2 contains exactly n ? k eigenvalues 
  //   of A.
  //
  // Examples
  // A = [
  //   10 -1 0 1
  //   0.2 8 0.2 0.2
  //   1 1 2 1
  //   -1 -1 -1 -11
  // ];
  // dispmat_gersh(A)
  // spec(A)
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //
  // Bibliography
  // http://en.wikipedia.org/wiki/Gershgorin_circle_theorem
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "dispmat_gersh" , rhs , 1 )
  apifun_checklhs ( "dispmat_gersh" , lhs , 1 )
  //
  apifun_checktype ( "dispmat_gersh" , A , "A" , 1 , "constant" )
  //
  h = scf()
  n  = size ( A , "r" )
  xmin = %inf
  ymin = %inf
  xmax = -%inf
  ymax = -%inf
  // Compute xmax, xmin, ymax, ymin
  for i = 1 : n
    r = sum(abs(A(i,:))) - abs(A(i,i));
    xmax = max ( xmax , real(A(i,i)) + r );
    xmin = min ( xmin , real(A(i,i)) - r );
    ymax = max ( ymax , imag(A(i,i)) + r );
    ymin = min ( ymin , imag(A(i,i)) - r );
  end
  isoview(xmin,xmax,ymin,ymax)
  // Draw the circles
  for i = 1 : n
    r = sum(abs(A(i,:))) - abs(A(i,i))
    xcircle ( real(A(i,i)) , imag(A(i,i)) , r )
    plot ( real(A(i,i)) , imag(A(i,i)) , "bx" )
  end
endfunction


// xcircle --
//   Draw a circle of radius r at given center x,y 
function xcircle ( x , y , r )
  xc = x - r
  yc = y + r
  xarc( xc , yc , 2*r , 2*r , 0 , 64 * 360 )
endfunction

