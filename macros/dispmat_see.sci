// Copyright (C) 2008 - 2010 - Michael Baudin

// dispmat_see --
//   Pictures of a matrix and its pseudo-inverse.
function h = dispmat_see ( A ) 
  //   Pictures of a matrix and its pseudo-inverse.
  // 
  // Calling Sequence
  // h = dispmat_see ( A )
  //
  // Parameters
  //   A : a n-by-m matrix of doubles
  //
  // Description
  //   Create a graphics with 4 subplots.
  //   Print A with 
  //   <itemizedlist>
  //   <listitem>a mesh, </listitem>
  //   <listitem>the pseudo-inverse of A as a mesh, </listitem>
  //   <listitem>the svd of A in log scale, </listitem>
  //   <listitem>the real and imaginary parts of the eigenvalues.</listitem>
  //   </itemizedlist>
  //
  // Examples
  // A=[
  //           -9          11         -21          63        -252
  //           70         -69         141        -421        1684
  //         -575         575       -1149        3451      -13801
  //         3891       -3891        7782      -23345       93365
  //         1024       -1024        2048       -6144       24572
  // ];
  // h = dispmat_see ( A );
  // // Take some time to see the matrix...
  // // ... now close
  // close ( h );
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //
  // Bibliograph
  //      "Algorithm 694: a collection of test matrices in MATLAB", Nicolas Higham, ACM Transactions on Mathematical Software, Volume 17 ,  Issue 3  (September 1991), Pages: 289 - 305
  //      "Higham's Test Matrices ", John Burkardt, 2005, http://people.sc.fsu.edu/~burkardt/m_src/test_matrix/test_matrix.html
  //      "The Test Matrix Toolbox for Matlab (Version 3.0)", Nicolas Higham, Numerical Analysis Report No. 276, September 1995, Manchester Centre for Computational Mathematics, The University of Manchester
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "dispmat_see" , rhs , 1 )
  apifun_checklhs ( "dispmat_see" , lhs , 1 )
  //
  apifun_checktype ( "dispmat_see" , A , "A" , 1 , "constant" )
  //
  // Print A
  h = scf()
  subplot(2,2,1)
  mesh(A)
  // Print the pseudo-inverse of A
  B = pinv(A);
  subplot(2,2,2)
  mesh(real(B))
  // Print the svd of A in log scale
  s = svd(A);
  subplot(2,2,3)
  plot(s, 'og')
  h = gcf()
  h.children(1).children.children.line_mode="on"
  h.children(1).log_flags="nln"
  // Plot the real and imaginary parts of the eigenvalues
  subplot(2,2,4)
  s = spec(A)
  plot(real(s),imag(s),"xb")
endfunction


